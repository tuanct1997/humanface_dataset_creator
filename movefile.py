import os
import random
import shutil
import math

source = 'archive/cropped'
dest = 'testB'
files = os.listdir(source)

no_of_files = math.ceil(150)

for file_name in random.sample(files, no_of_files):
	try:
		shutil.move(os.path.join(source, file_name), dest)
	except:
		continue