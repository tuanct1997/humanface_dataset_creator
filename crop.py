import cv2
import sys
import os
# face detection with mtcnn on a photograph
from matplotlib import pyplot
from matplotlib.patches import Rectangle
from mtcnn.mtcnn import MTCNN

folder = sys.argv[1]
i = 0
for imagepath in os.listdir(folder):

	image = cv2.imread(os.path.join(folder,imagepath))
	try:
		gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	except :
		continue

	faceCascade = cv2.CascadeClassifier(cv2.data.haarcascades + "haarcascade_frontalface_default.xml")
	faces = faceCascade.detectMultiScale(
	    gray,
	    scaleFactor=1.3,
	    minNeighbors=3,
	    minSize=(30, 30)
	)

	print("[INFO] Found {0} Faces!".format(len(faces)))

	if len(faces) == 0:
		continue

	for (x, y, w, h) in faces:
	    cv2.rectangle(image, (x-60, y-60), (x + w +60, y + h +60), (0, 255, 0), 2)
	    roi_color = image[y:y + h, x:x + w]
	    print("[INFO] Object found. Saving locally.")
	    cv2.imwrite('crop/'+str(i) + '_faces.jpg', roi_color)

	i += 1
	status = cv2.imwrite('faces_detected.jpg', image)
	print("[INFO] Image faces_detected.jpg written to filesystem: ", status)