# Humanface_dataset_Creator

## Introduction
This project aim to create a tool that has ability to crawl dataset from BingAPI. This repository contain repository `Google_image_download` from [](https://github.com/ultralytics/google-images-download) in order to access and crawl images from BingAPI. I personally thank [ultralytics](https://github.com/ultralytics) for providing this repository when base google_images_download library on pip hasn't working. 

## Requirements
+ Python 3.8 or above
+ OpenCV
+ Keras

## Setup

```
$ pip install -r requirements.txt
$ pip install keras
$ pip install opencv-python
```
This project requires Google Chrome
+ Install/update Chrome: https://www.google.com/chrome/

+ Install/update chromedriver: https://chromedriver.chromium.org/
## How to run?

In order to crawl images from BingAPI: 

```
$ cd google_images_download
$ python bing_scraper.py --search <Search string> --limit <Number_of_images_want_to_Crawl> --download --chromedriver ./chromedriver
## Movefile if needed ( When you want to organize folder)
$ python movefile.py
## Create npz dataset from images
$ python process_data.py
```
